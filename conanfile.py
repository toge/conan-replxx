from conans import ConanFile, CMake, tools
import os

required_conan_version = ">=1.33.0"

class ReplxxConan(ConanFile):
    name            = "replxx"
    license         = "mixed"
    url             = "https://bitbucket.org/toge/conan-replxx/"
    description     = "Read Evaluate Print Loop ++"
    settings        = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake", "cmake_find_package", "cmake_find_package_multi"

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    def source(self):
        tools.get(**self.conan_data["sources"][self.version], destination=self._source_subfolder, strip_root=True)

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["REPLXX_BUILD_TESTS"] = False
        cmake.definitions["REPLXX_BUILD_EXAMPLES"] = False
        cmake.configure(source_folder=self._source_subfolder)
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        self.copy("LICENSE*", "licenses", self._source_subfolder)

        cmake = self._configure_cmake()
        cmake.install()

        tools.rmdir(os.path.join(self.package_folder, "share"))

    def package_info(self):
        if self.settings.os == "Linux":
            self.cpp_info.system_libs.append("pthread")
        self.cpp_info.libs = ["replxx"]
        self.cpp_info.filenames["cmake_find_package"] = "replxx"
        self.cpp_info.filenames["cmake_find_package_multi"] = "replxx"
